package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.entity.SalesOrderHeader;
import com.mycompany.myapp.service.SalesOrderHeaderService;

@RestController
public class SalesOrderHeaderRWS
{
	private static final Logger logger = LoggerFactory.getLogger(SalesOrderHeaderRWS.class);
	
	@Autowired
	SalesOrderHeaderService salesOrderHeaderService; 
	
	@RequestMapping(value = "/salesorderheader/{id}", method = RequestMethod.GET)
	public ResponseEntity<SalesOrderHeader> findOne(@PathVariable Integer id)
	{
		logger.info("Listing salesorderheader with id: " + id);
		return new ResponseEntity<SalesOrderHeader>(salesOrderHeaderService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/salesorderheader", method = RequestMethod.GET)
	public ResponseEntity<List<SalesOrderHeader>> findAll()
	{
		logger.info("Listing all salesorderheaders");
		return new ResponseEntity<List<SalesOrderHeader>>(salesOrderHeaderService.findAll(), HttpStatus.OK);
	}

}

package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.entity.SalesOrderDetail;
import com.mycompany.myapp.service.SalesOrderDetailService;

@RestController
public class SalesOrderDetailsRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(SalesOrderDetailsRWS.class);
	
	@Autowired
	SalesOrderDetailService salesOrderDetailsService; 
	
	@RequestMapping(value = "/salesorderdetail/{salesOrderID}/{salesOrderDetailID}", method = RequestMethod.GET)
	public ResponseEntity<SalesOrderDetail> findOne(@PathVariable Integer salesOrderID, @PathVariable Integer salesOrderDetailID)
	{
		logger.info("Listing salesorderdetail with salesOrderID: " + salesOrderID + " and salesOrderDetailID: " + salesOrderDetailID);
		return new ResponseEntity<SalesOrderDetail>(salesOrderDetailsService.findOne(salesOrderID, salesOrderDetailID), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/salesorderdetails", method = RequestMethod.GET)
	public ResponseEntity<List<SalesOrderDetail>> findAll()
	{
		logger.info("Listing all salesorderheaders");
		return new ResponseEntity<List<SalesOrderDetail>>(salesOrderDetailsService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/salesorderdetails/{pageNum}/{pageSize}", method = RequestMethod.GET)
	public ResponseEntity<Page<SalesOrderDetail>> findAllWithPaging(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
		logger.info("Listing all addresses with paging");
		return new ResponseEntity<Page<SalesOrderDetail>>(salesOrderDetailsService.findAllWithPaging(pageNum, pageSize), HttpStatus.OK);
	}
	
	

}

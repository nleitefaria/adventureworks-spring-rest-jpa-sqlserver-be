package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.dto.ProductDescriptionDTO;
import com.mycompany.myapp.entity.ProductDescription;
import com.mycompany.myapp.service.ProductDescriptionService;

@RestController
public class ProductDescriptionRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(ProductDescriptionRWS.class);
	
	@Autowired
	ProductDescriptionService productDescriptionService; 
	
	@RequestMapping(value = "/productdescription/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProductDescription> findOne(@PathVariable Integer id)
	{
		logger.info("Listing productDescription with id: " + id);
		return new ResponseEntity<ProductDescription>(productDescriptionService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/productdescriptions", method = RequestMethod.GET)
	public ResponseEntity<List<ProductDescription>> findAll() {
		logger.info("Listing all productDescriptions");
		return new ResponseEntity<List<ProductDescription>>(productDescriptionService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/productdescription", method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestBody ProductDescriptionDTO productDescriptionDTO)
	{
		logger.info("Creating product description");
		productDescriptionService.save(productDescriptionDTO);
		logger.info("Done");
		return new ResponseEntity<ProductDescriptionDTO>(productDescriptionDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/productdescription/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody ProductDescriptionDTO productDescriptionDTO)
	{
		if(productDescriptionService.update(id, productDescriptionDTO) != null)
		{
			return ResponseEntity.ok("resource updated");
		}
		else
		{
			return ResponseEntity.notFound().build();
		}		
	}

}

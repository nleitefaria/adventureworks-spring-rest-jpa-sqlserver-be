package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.dto.ProductDTO;
import com.mycompany.myapp.entity.Product;
import com.mycompany.myapp.service.ProductService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ProductRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(ProductRWS.class);
	
	@Autowired
	ProductService productService; 
	
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public ResponseEntity<Product> findOne(@PathVariable Integer id)
	{
		logger.info("Listing product with id: " + id);
		return new ResponseEntity<Product>(productService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> findAll()
	{
		logger.info("Listing all products");
		return new ResponseEntity<List<Product>>(productService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/products/{pageNum}/{pageSize}", method = RequestMethod.GET)
	public ResponseEntity<Page<Product>> findAllWithPaging(@PathVariable String pageNum, @PathVariable Integer pageSize) {
		logger.info("Listing all products with paging");
		return new ResponseEntity<Page<Product>>(productService.findAllWithPaging(Integer.parseInt(pageNum), pageSize), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestBody ProductDTO productDTO)
	{
		logger.info("Creating product");
		productService.save(productDTO);
		logger.info("Done");
		return new ResponseEntity<ProductDTO>(productDTO, HttpStatus.CREATED);
	}

}

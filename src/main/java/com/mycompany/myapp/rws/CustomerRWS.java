package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.entity.Customer;
import com.mycompany.myapp.service.CustomerService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerRWS
{
	private static final Logger logger = LoggerFactory.getLogger(CustomerRWS.class);
	
	@Autowired
	CustomerService customerService; 
	
	@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
	public ResponseEntity<Customer> findOne(@PathVariable Integer id)
	{
		logger.info("Listing customer with id: " + id);
		return new ResponseEntity<Customer>(customerService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/customers", method = RequestMethod.GET)
	public ResponseEntity<List<Customer>> findAll() 
	{
		logger.info("Listing all customers");
		return new ResponseEntity<List<Customer>>(customerService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/customers/{pageNum}/{pageSize}", method = RequestMethod.GET)
	public ResponseEntity<Page<Customer>> findAllWithPaging(@PathVariable String pageNum, @PathVariable Integer pageSize) {
		logger.info("pageNum: " + pageNum);
		logger.info("Listing all customers with paging");
		return new ResponseEntity<Page<Customer>>(customerService.findAllWithPaging(Integer.parseInt(pageNum), pageSize), HttpStatus.OK);
		
	}

}

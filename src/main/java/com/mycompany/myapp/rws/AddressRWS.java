package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.dto.AddressDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.service.AddressService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AddressRWS
{
	private static final Logger logger = LoggerFactory.getLogger(AddressRWS.class);
	
	@Autowired
	AddressService addressService; 
	
	@RequestMapping(value = "/address/{id}", method = RequestMethod.GET)
	public ResponseEntity<Address> findOne(@PathVariable Integer id)
	{
		logger.info("Listing address with id: " + id);
		return new ResponseEntity<Address>(addressService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addresses", method = RequestMethod.GET)
	public ResponseEntity<List<Address>> findAll()
	{
		logger.info("Listing all addresses");
		return new ResponseEntity<List<Address>>(addressService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addresses/{pageNum}/{pageSize}", method = RequestMethod.GET)
	public ResponseEntity<Page<Address>> findAllWithPaging(@PathVariable String pageNum, @PathVariable Integer pageSize) {
		logger.info("Listing all addresses with paging");
		return new ResponseEntity<Page<Address>>(addressService.findAllWithPaging(Integer.parseInt(pageNum), pageSize), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/address", method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestBody AddressDTO addressDTO)
	{
		logger.info("Creating address");
		addressService.save(addressDTO);
		logger.info("Done");
		return new ResponseEntity<AddressDTO>(addressDTO, HttpStatus.CREATED);
	}

}

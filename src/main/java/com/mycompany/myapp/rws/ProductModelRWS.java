package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.dto.ProductModelDTO;
import com.mycompany.myapp.entity.ProductModel;
import com.mycompany.myapp.service.ProductModelService;

@RestController
public class ProductModelRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(ProductModelRWS.class);
	
	@Autowired
	ProductModelService productModelService; 
	
	@RequestMapping(value = "/productmodel/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProductModel> findOne(@PathVariable Integer id)
	{
		logger.info("Listing productmodel with id: " + id);
		return new ResponseEntity<ProductModel>(productModelService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/productmodels", method = RequestMethod.GET)
	public ResponseEntity<List<ProductModel>> findAll() {
		logger.info("Listing all productmodels");
		return new ResponseEntity<List<ProductModel>>(productModelService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/productmodel", method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestBody ProductModelDTO productModelDTO)
	{
		logger.info("Creating productmodel");
		productModelService.save(productModelDTO);
		logger.info("Done");
		return new ResponseEntity<ProductModelDTO>(productModelDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/productmodel/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody ProductModelDTO productModelDTO)
	{
		if(productModelService.update(id, productModelDTO) != null)
		{
			return ResponseEntity.ok("resource updated");
		}
		else
		{
			return ResponseEntity.notFound().build();
		}		
	}

}

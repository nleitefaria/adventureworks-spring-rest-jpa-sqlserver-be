package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.dto.ProductCategoryDTO;
import com.mycompany.myapp.entity.ProductCategory;
import com.mycompany.myapp.service.ProductCategoryService;

@RestController
public class ProductCategoryRWS
{
	private static final Logger logger = LoggerFactory.getLogger(ProductCategoryRWS.class);
	
	@Autowired
	ProductCategoryService productCategoryService; 
	
	@RequestMapping(value = "/productcategory/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProductCategory> findOne(@PathVariable Integer id)
	{
		logger.info("Listing product category with id: " + id);
		return new ResponseEntity<ProductCategory>(productCategoryService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/productcategories", method = RequestMethod.GET)
	public ResponseEntity<List<ProductCategory>> findAll() {
		logger.info("Listing all product categories");
		return new ResponseEntity<List<ProductCategory>>(productCategoryService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/productcategory", method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestBody ProductCategoryDTO productCategoryDTO)
	{
		logger.info("Creating product category");
		productCategoryService.save(productCategoryDTO);
		logger.info("Done");
		return new ResponseEntity<ProductCategoryDTO>(productCategoryDTO, HttpStatus.CREATED);
	}

}

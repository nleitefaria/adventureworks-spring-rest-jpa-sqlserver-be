package com.mycompany.myapp.dto;

import java.util.Date;

public class ProductDescriptionDTO 
{	
    private String description; 
    private String rowguid;
    private Date modifiedDate;
    
    public ProductDescriptionDTO()
    {    
    }
    
    public ProductDescriptionDTO(String description, String rowguid, Date modifiedDate)
    {  
        this.description = description;
        this.rowguid = rowguid;
        this.modifiedDate = modifiedDate;
    }
    
    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}

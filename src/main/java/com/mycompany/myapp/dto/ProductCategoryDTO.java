package com.mycompany.myapp.dto;

import java.util.Date;



public class ProductCategoryDTO 
{	
	//private Integer productCategoryID;
	private String name;
    private String rowguid;
    private Date modifiedDate;
	
	public ProductCategoryDTO()
	{
	}
	
	public ProductCategoryDTO(String name, String rowguid, Date modifiedDate) 
	{	
		this.name = name;
		this.rowguid = rowguid;
		this.modifiedDate = modifiedDate;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getRowguid() 
	{
		return rowguid;
	}

	public void setRowguid(String rowguid) 
	{
		this.rowguid = rowguid;
	}

	public Date getModifiedDate() 
	{
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}	
}

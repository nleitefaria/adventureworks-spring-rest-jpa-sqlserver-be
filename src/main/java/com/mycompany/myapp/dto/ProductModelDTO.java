package com.mycompany.myapp.dto;

import java.util.Date;

public class ProductModelDTO 
{
    private String name;
    private String catalogDescription;
    private String rowguid;
    private Date modifiedDate;
    
    public ProductModelDTO() {
	}
    
	public ProductModelDTO(String name, String catalogDescription, String rowguid, Date modifiedDate) {
		this.name = name;
		this.catalogDescription = catalogDescription;
		this.rowguid = rowguid;
		this.modifiedDate = modifiedDate;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCatalogDescription() {
		return catalogDescription;
	}


	public void setCatalogDescription(String catalogDescription) {
		this.catalogDescription = catalogDescription;
	}


	public String getRowguid() {
		return rowguid;
	}


	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}


	public Date getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}

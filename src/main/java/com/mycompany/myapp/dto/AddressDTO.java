package com.mycompany.myapp.dto;

import java.util.Date;

public class AddressDTO
{
	private String addressLine1; 
	private String addressLine2;
	private String city;
	private String stateProvince;
	private String countryRegion;
	private String postalCode;
	private String rowguid;
	private Date modifiedDate;
		
	public AddressDTO()
	{		
	}
	
	public AddressDTO(String addressLine1, String addressLine2, String city, String stateProvince, String countryRegion, String postalCode, String rowguid, Date modifiedDate) 
	{
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.stateProvince = stateProvince;
		this.countryRegion = countryRegion;
		this.postalCode = postalCode;
		this.rowguid = rowguid;
		this.modifiedDate = modifiedDate;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getCountryRegion() {
		return countryRegion;
	}

	public void setCountryRegion(String countryRegion) {
		this.countryRegion = countryRegion;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}

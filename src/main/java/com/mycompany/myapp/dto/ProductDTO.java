package com.mycompany.myapp.dto;

import java.math.BigDecimal;
import java.util.Date;

public class ProductDTO 
{
	private String name;
	private String productNumber;
	private BigDecimal standardCost;
	private BigDecimal listPrice;
	private Date sellStartDate;
	private String rowguid;
	private Date modifiedDate;
	
	public ProductDTO()
	{
	}
	
	public ProductDTO(String name, String productNumber, BigDecimal standardCost, BigDecimal listPrice, Date sellStartDate, String rowguid, Date modifiedDate) 
    {
        this.name = name;
        this.productNumber = productNumber;
        this.standardCost = standardCost;
        this.listPrice = listPrice;
        this.sellStartDate = sellStartDate;
        this.rowguid = rowguid;
        this.modifiedDate = modifiedDate;
    }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}

	public BigDecimal getStandardCost() {
		return standardCost;
	}

	public void setStandardCost(BigDecimal standardCost) {
		this.standardCost = standardCost;
	}

	public BigDecimal getListPrice() {
		return listPrice;
	}

	public void setListPrice(BigDecimal listPrice) {
		this.listPrice = listPrice;
	}

	public Date getSellStartDate() {
		return sellStartDate;
	}

	public void setSellStartDate(Date sellStartDate) {
		this.sellStartDate = sellStartDate;
	}

	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
}

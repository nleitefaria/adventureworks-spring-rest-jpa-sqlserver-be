package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.mycompany.myapp.entity.ProductDescription;

@Repository
public interface ProductDescriptionRepository  extends JpaRepository<ProductDescription, Integer>{

}

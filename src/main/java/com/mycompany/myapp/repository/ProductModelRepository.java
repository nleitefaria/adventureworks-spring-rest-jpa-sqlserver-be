package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.entity.ProductModel;

@Repository
public interface ProductModelRepository extends JpaRepository<ProductModel, Integer>{

}

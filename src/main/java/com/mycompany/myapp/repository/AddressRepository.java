package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>{

}

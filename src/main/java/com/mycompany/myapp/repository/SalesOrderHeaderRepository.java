package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.entity.SalesOrderHeader;

@Repository
public interface SalesOrderHeaderRepository extends JpaRepository<SalesOrderHeader, Integer>{

}

package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mycompany.myapp.entity.SalesOrderDetail;
import com.mycompany.myapp.entity.SalesOrderDetailPK;

public interface SalesOrderDetailRepository extends JpaRepository<SalesOrderDetail, SalesOrderDetailPK>
{

}

package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.myapp.dto.AddressDTO;
import com.mycompany.myapp.entity.Address;

public interface AddressService {
	
	long count();
	Address findOne(Integer id);
	List<Address> findAll();
	Page<Address> findAllWithPaging(int page, int size);
	AddressDTO save(AddressDTO addressDTO);

}

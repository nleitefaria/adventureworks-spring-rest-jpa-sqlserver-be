package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.myapp.entity.Customer;

public interface CustomerService {
	
	long count();
	Customer findOne(Integer id);
	List<Customer> findAll();
	Page<Customer> findAllWithPaging(int page, int size);

}

package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.myapp.entity.SalesOrderDetail;

public interface SalesOrderDetailService 
{
	long count();
	SalesOrderDetail findOne(Integer salesOrderID, Integer salesOrderDetailID);
	List<SalesOrderDetail> findAll();
	Page<SalesOrderDetail> findAllWithPaging(int page, int size);


}

package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.entity.SalesOrderHeader;

public interface SalesOrderHeaderService 
{
	long count();
	SalesOrderHeader findOne(Integer id);
	List<SalesOrderHeader> findAll();
}

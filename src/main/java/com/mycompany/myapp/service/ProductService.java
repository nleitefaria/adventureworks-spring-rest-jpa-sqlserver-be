package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.myapp.dto.ProductDTO;
import com.mycompany.myapp.entity.Product;

public interface ProductService 
{
	long count();
	Product findOne(Integer id);
	List<Product> findAll();
	Page<Product> findAllWithPaging(int page, int size);
	ProductDTO save(ProductDTO productDTO);
}

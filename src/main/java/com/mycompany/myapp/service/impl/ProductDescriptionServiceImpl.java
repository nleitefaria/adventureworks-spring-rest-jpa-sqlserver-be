package com.mycompany.myapp.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.dto.ProductDescriptionDTO;
import com.mycompany.myapp.entity.ProductDescription;
import com.mycompany.myapp.repository.ProductDescriptionRepository;
import com.mycompany.myapp.service.ProductDescriptionService;

@Service
public class ProductDescriptionServiceImpl implements ProductDescriptionService
{	
	@Autowired
	ProductDescriptionRepository productDescriptionRepository;

	@Transactional
	public long count() 
	{		
		return productDescriptionRepository.count();		
	}
	
	@Transactional
	public ProductDescription findOne(Integer id) 
	{		
		return  productDescriptionRepository.findOne(id);	
	}
	
	@Transactional
	public List<ProductDescription> findAll() 
	{
		return  productDescriptionRepository.findAll();	
	}

	@Transactional
	public ProductDescriptionDTO save(ProductDescriptionDTO productDescriptionDTO) 
	{	
		productDescriptionRepository.save(toProductDescription(productDescriptionDTO));
		return productDescriptionDTO;
	}
	
	@Transactional
	public ProductDescriptionDTO update(String id, ProductDescriptionDTO productDescriptionDTO) 
	{			
		ProductDescription productDescription = productDescriptionRepository.findOne(Integer.parseInt(id));
		
	    if(productDescription == null)
	    {
	    	return null; 
	    }	    
	    
	    productDescription.setDescription(productDescriptionDTO.getDescription());
	    productDescription.setRowguid(productDescriptionDTO.getRowguid());
	    productDescription.setModifiedDate(new Date());
	    
	    ProductDescription productDescriptionUpdated = productDescriptionRepository.save(productDescription);
	    
	    productDescriptionDTO.setDescription(productDescriptionUpdated.getDescription());
	    productDescriptionDTO.setRowguid(productDescriptionUpdated.getRowguid());
	    productDescriptionDTO.setModifiedDate(productDescriptionUpdated.getModifiedDate());
		
		return productDescriptionDTO;
	}
	
	
	private ProductDescription toProductDescription(ProductDescriptionDTO productDescriptionDTO)
	{		
		return new ProductDescription(productDescriptionDTO.getDescription(), productDescriptionDTO.getRowguid(), productDescriptionDTO.getModifiedDate());		
	}

	

}

package com.mycompany.myapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.dto.ProductCategoryDTO;
import com.mycompany.myapp.entity.ProductCategory;
import com.mycompany.myapp.repository.ProductCategoryRepository;
import com.mycompany.myapp.service.ProductCategoryService;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService
{
	@Autowired
	ProductCategoryRepository productCategoryRepository;

	@Transactional
	public long count() 
	{		
		return productCategoryRepository.count();		
	}
	
	@Transactional
	public ProductCategory findOne(Integer id) 
	{		
		return  productCategoryRepository.findOne(id);	
	}
	
	@Transactional
	public List<ProductCategory> findAll() 
	{
		return  productCategoryRepository.findAll();	
	}
	
	@Transactional
	public ProductCategoryDTO save(ProductCategoryDTO productCategoryDTO) 
	{	
		productCategoryRepository.save(toProductCategory(productCategoryDTO));
		return productCategoryDTO;
	}
	
	private ProductCategory toProductCategory(ProductCategoryDTO productCategoryDTO)
	{
		ProductCategory productCategory = new ProductCategory(productCategoryDTO.getName(), productCategoryDTO.getRowguid(), productCategoryDTO.getModifiedDate());	
		return productCategory;
	}

}

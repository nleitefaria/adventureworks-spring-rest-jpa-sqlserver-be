package com.mycompany.myapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.entity.SalesOrderHeader;
import com.mycompany.myapp.repository.SalesOrderHeaderRepository;
import com.mycompany.myapp.service.SalesOrderHeaderService;

@Service
public class SalesOrderHeaderServiceImpl implements SalesOrderHeaderService
{	
	@Autowired
	SalesOrderHeaderRepository salesOrderHeaderRepository;

	@Transactional
	public long count() 
	{		
		return salesOrderHeaderRepository.count();		
	}
	
	@Transactional
	public SalesOrderHeader findOne(Integer id) 
	{		
		return  salesOrderHeaderRepository.findOne(id);	
	}
	
	@Transactional
	public List<SalesOrderHeader> findAll() 
	{
		return  salesOrderHeaderRepository.findAll();	
	}

}

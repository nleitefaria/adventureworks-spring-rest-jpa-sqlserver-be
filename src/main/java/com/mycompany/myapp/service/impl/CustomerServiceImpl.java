package com.mycompany.myapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.entity.Customer;
import com.mycompany.myapp.repository.CustomerRepository;
import com.mycompany.myapp.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService
{
	@Autowired
	CustomerRepository customerRepository;

	@Transactional
	public long count() 
	{		
		return customerRepository.count();		
	}
	
	@Transactional
	public Customer findOne(Integer id) 
	{		
		return customerRepository.findOne(id);	
	}
	
	@Transactional
	public List<Customer> findAll() 
	{
		return customerRepository.findAll();	
	}
	
	@Transactional
	public Page<Customer> findAllWithPaging(int page, int size) 
	{
		return customerRepository.findAll(createPageRequest(page, size));
	}
	
	private Pageable createPageRequest(int page, int size)
	{
	    return new PageRequest(page, size);
	}
	
	

}

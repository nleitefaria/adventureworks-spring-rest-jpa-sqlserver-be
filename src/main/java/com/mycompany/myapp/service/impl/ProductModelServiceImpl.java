package com.mycompany.myapp.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.dto.ProductModelDTO;
import com.mycompany.myapp.entity.ProductModel;
import com.mycompany.myapp.repository.ProductModelRepository;
import com.mycompany.myapp.service.ProductModelService;

@Service
public class ProductModelServiceImpl implements ProductModelService
{	
	@Autowired
	ProductModelRepository productModelRepository;

	@Transactional
	public long count() 
	{		
		return productModelRepository.count();		
	}
	
	@Transactional
	public ProductModel findOne(Integer id) 
	{		
		return  productModelRepository.findOne(id);	
	}
	
	@Transactional
	public List<ProductModel> findAll() 
	{
		return  productModelRepository.findAll();	
	}
	
	@Transactional
	public ProductModelDTO save(ProductModelDTO productModelDTO) 
	{	
		productModelRepository.save(toProductModel(productModelDTO));
		return productModelDTO;
	}
	
	@Transactional
	public ProductModelDTO update(String id, ProductModelDTO productModelDTO) 
	{			
		ProductModel productModel = productModelRepository.findOne(Integer.parseInt(id));
		
	    if(productModel == null)
	    {
	    	return null; 
	    }
	    
	    productModel.setName(productModelDTO.getName());
	    productModel.setCatalogDescription(productModelDTO.getCatalogDescription());
	    productModel.setRowguid(productModelDTO.getRowguid());
	    productModel.setModifiedDate(new Date());
	    
	    ProductModel productModelUpdated = productModelRepository.save(productModel);
	    
	    productModelDTO.setName(productModelUpdated.getName());
	    productModelDTO.setCatalogDescription(productModelUpdated.getCatalogDescription());
	    productModelDTO.setRowguid(productModelUpdated.getRowguid());
	    productModelDTO.setModifiedDate(productModelUpdated.getModifiedDate());
		
		return productModelDTO;
	}

	private ProductModel toProductModel(ProductModelDTO productModelDTO)
	{		
		return new ProductModel(productModelDTO.getName(), productModelDTO.getRowguid(), productModelDTO.getModifiedDate());
	}
	
	
}

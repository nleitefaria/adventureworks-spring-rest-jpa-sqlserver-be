package com.mycompany.myapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.dto.ProductDTO;
import com.mycompany.myapp.entity.Product;
import com.mycompany.myapp.repository.ProductRepository;
import com.mycompany.myapp.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService
{
	@Autowired
	ProductRepository productRepository;

	@Transactional
	public long count() 
	{		
		return productRepository.count();		
	}
	
	@Transactional
	public Product findOne(Integer id) 
	{		
		return  productRepository.findOne(id);	
	}
	
	@Transactional
	public List<Product> findAll() 
	{
		return  productRepository.findAll();	
	}
	
	@Transactional
	public Page<Product> findAllWithPaging(int page, int size) 
	{
		return productRepository.findAll(createPageRequest(page, size));
	}
	
	@Transactional
	public ProductDTO save(ProductDTO productDTO) 
	{
		productRepository.save(toProduct(productDTO));
		return productDTO;
	}
	
	private Pageable createPageRequest(int page, int size)
	{
	    return new PageRequest(page, size);
	}

	private Product toProduct(ProductDTO productDTO)
	{			
		return new Product(productDTO.getName(), productDTO.getProductNumber(), productDTO.getStandardCost(), productDTO.getListPrice(), productDTO.getSellStartDate(), productDTO.getRowguid(), productDTO.getModifiedDate());					
	}

}

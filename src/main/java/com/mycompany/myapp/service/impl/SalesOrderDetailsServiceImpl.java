package com.mycompany.myapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.entity.SalesOrderDetail;
import com.mycompany.myapp.entity.SalesOrderDetailPK;
import com.mycompany.myapp.repository.SalesOrderDetailRepository;
import com.mycompany.myapp.service.SalesOrderDetailService;

@Service
public class SalesOrderDetailsServiceImpl implements SalesOrderDetailService {
	
	@Autowired
	SalesOrderDetailRepository salesOrderDetailsRepository;

	@Transactional
	public long count() 
	{		
		return salesOrderDetailsRepository.count();		
	}
	
	@Transactional
	public SalesOrderDetail findOne(Integer salesOrderID, Integer salesOrderDetailID) 
	{		
		SalesOrderDetailPK id = new SalesOrderDetailPK(salesOrderID, salesOrderDetailID);
		return salesOrderDetailsRepository.findOne(id);	
	}
	
	@Transactional
	public List<SalesOrderDetail> findAll() 
	{
		return  salesOrderDetailsRepository.findAll();	
	}
	
	@Transactional
	public Page<SalesOrderDetail> findAllWithPaging(int page, int size) 
	{
		return salesOrderDetailsRepository.findAll(createPageRequest(page, size));
	}
	
	private Pageable createPageRequest(int page, int size)
	{
	    return new PageRequest(page - 1, size);
	}

	
	
}

package com.mycompany.myapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.dto.AddressDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.repository.AddressRepository;
import com.mycompany.myapp.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService 
{
	@Autowired
	AddressRepository addressRepository;

	@Transactional
	public long count() 
	{		
		return addressRepository.count();		
	}
	
	@Transactional
	public Address findOne(Integer id) 
	{		
		return  addressRepository.findOne(id);	
	}
	
	@Transactional
	public List<Address> findAll() 
	{
		return  addressRepository.findAll();	
	}
		
	@Transactional
	public Page<Address> findAllWithPaging(int page, int size) 
	{
		return addressRepository.findAll(createPageRequest(page, size));
	}
	
	@Transactional
	public AddressDTO save(AddressDTO addressDTO) 
	{	
		addressRepository.save(toAddress(addressDTO));
		return addressDTO;
	}
	
	private Pageable createPageRequest(int page, int size)
	{
	    return new PageRequest(page - 1, size);
	}
	
	private Address toAddress(AddressDTO addressDTO)
	{
		Address address = new Address(addressDTO.getAddressLine1(), addressDTO.getCity(), addressDTO.getStateProvince(), addressDTO.getCountryRegion(), addressDTO.getPostalCode(), addressDTO.getRowguid(), addressDTO.getModifiedDate());	
		return address;
	}
}

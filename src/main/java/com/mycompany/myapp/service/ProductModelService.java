package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.dto.ProductModelDTO;
import com.mycompany.myapp.entity.ProductModel;

public interface ProductModelService
{	
	long count();
	ProductModel findOne(Integer id);
	List<ProductModel> findAll();
	ProductModelDTO save(ProductModelDTO productModelDTO);
	ProductModelDTO update(String id, ProductModelDTO productModelDTO);
}

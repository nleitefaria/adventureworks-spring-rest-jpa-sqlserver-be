package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.dto.ProductDescriptionDTO;
import com.mycompany.myapp.entity.ProductDescription;

public interface ProductDescriptionService
{
	long count();
	ProductDescription findOne(Integer id);
	List<ProductDescription> findAll();
	ProductDescriptionDTO save(ProductDescriptionDTO productDescriptionDTO);
	ProductDescriptionDTO update(String id, ProductDescriptionDTO productDescriptionDTO);
	

}

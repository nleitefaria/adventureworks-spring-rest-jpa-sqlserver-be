package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.dto.ProductCategoryDTO;
import com.mycompany.myapp.entity.ProductCategory;

public interface ProductCategoryService 
{	
	long count();
	ProductCategory findOne(Integer id);
	List<ProductCategory> findAll();
	ProductCategoryDTO save(ProductCategoryDTO productCategoryDTO);

}

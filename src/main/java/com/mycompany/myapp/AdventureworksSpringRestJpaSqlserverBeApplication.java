package com.mycompany.myapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdventureworksSpringRestJpaSqlserverBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdventureworksSpringRestJpaSqlserverBeApplication.class, args);
	}
}

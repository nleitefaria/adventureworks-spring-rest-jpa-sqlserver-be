/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "CustomerAddress", catalog = "AdventureWorksLT2012", schema = "SalesLT")
@NamedQueries({
    @NamedQuery(name = "CustomerAddress.findAll", query = "SELECT c FROM CustomerAddress c")
    , @NamedQuery(name = "CustomerAddress.findByCustomerID", query = "SELECT c FROM CustomerAddress c WHERE c.customerAddressPK.customerID = :customerID")
    , @NamedQuery(name = "CustomerAddress.findByAddressID", query = "SELECT c FROM CustomerAddress c WHERE c.customerAddressPK.addressID = :addressID")
    , @NamedQuery(name = "CustomerAddress.findByAddressType", query = "SELECT c FROM CustomerAddress c WHERE c.addressType = :addressType")
    , @NamedQuery(name = "CustomerAddress.findByRowguid", query = "SELECT c FROM CustomerAddress c WHERE c.rowguid = :rowguid")
    , @NamedQuery(name = "CustomerAddress.findByModifiedDate", query = "SELECT c FROM CustomerAddress c WHERE c.modifiedDate = :modifiedDate")})
public class CustomerAddress implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CustomerAddressPK customerAddressPK;
    @Basic(optional = false)
    @Column(name = "AddressType")
    private String addressType;
    @Basic(optional = false)
    @Column(name = "rowguid")
    private String rowguid;
    @Basic(optional = false)
    @Column(name = "ModifiedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;
    @JoinColumn(name = "AddressID", referencedColumnName = "AddressID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Address address;
    @JoinColumn(name = "CustomerID", referencedColumnName = "CustomerID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Customer customer;

    public CustomerAddress() {
    }

    public CustomerAddress(CustomerAddressPK customerAddressPK) {
        this.customerAddressPK = customerAddressPK;
    }

    public CustomerAddress(CustomerAddressPK customerAddressPK, String addressType, String rowguid, Date modifiedDate) {
        this.customerAddressPK = customerAddressPK;
        this.addressType = addressType;
        this.rowguid = rowguid;
        this.modifiedDate = modifiedDate;
    }

    public CustomerAddress(int customerID, int addressID) {
        this.customerAddressPK = new CustomerAddressPK(customerID, addressID);
    }

    public CustomerAddressPK getCustomerAddressPK() {
        return customerAddressPK;
    }

    public void setCustomerAddressPK(CustomerAddressPK customerAddressPK) {
        this.customerAddressPK = customerAddressPK;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getRowguid() {
        return rowguid;
    }

    public void setRowguid(String rowguid) {
        this.rowguid = rowguid;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customerAddressPK != null ? customerAddressPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerAddress)) {
            return false;
        }
        CustomerAddress other = (CustomerAddress) object;
        if ((this.customerAddressPK == null && other.customerAddressPK != null) || (this.customerAddressPK != null && !this.customerAddressPK.equals(other.customerAddressPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.CustomerAddress[ customerAddressPK=" + customerAddressPK + " ]";
    }
    
}
